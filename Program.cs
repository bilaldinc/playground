﻿using Minio;
using Minio.DataModel;

string endpoint = "127.0.1.1";
// string endpoint = "192.168.1.33";
int port = 9000;
string accessKey = "7cd2e739-644e-48b1-81bb-5098120888b3";
string secretKey = "b6f3920b-f3b9-4bb0-add5-0c42c28bbbfb";

var minio = new MinioClient()
    .WithEndpoint(endpoint, port)
    .WithCredentials(accessKey, secretKey)
    .Build();

async Task UploadFile(MinioClient minio)
{
    FileInfo fileInfo = new FileInfo("file.jpeg");
    FileStream fs = fileInfo.OpenRead();

    string bucketName = "test";
    string objectName = Guid.NewGuid().ToString() + fileInfo.Extension;

    var objectArgs = new PutObjectArgs()
        .WithBucket(bucketName)
        .WithObject(objectName)
        .WithObjectSize(fs.Length)
        .WithContentType("image/jpeg")
        .WithStreamData(fs);

    await minio.PutObjectAsync(objectArgs);

    fs.Close();
}

async Task GetFile(MinioClient minio)
{
    string bucketName = "test";
    string objectName = "3e6f3aa5-b9b5-4f6c-a5ce-09895638197d.jpeg";

    FileInfo fileInfo = new FileInfo("hello.jpeg");
    FileStream fs = fileInfo.OpenWrite();

    var objectArgs = new GetObjectArgs()
        .WithBucket(bucketName)
        .WithObject(objectName)
        .WithCallbackStream(cb => cb.CopyTo(fs));

    var result = await minio.GetObjectAsync(objectArgs);

    fs.Close();
}

async Task GetFileUrl(MinioClient minio)
{
    string bucketName = "test";
    string objectName = "3e6f3aa5-b9b5-4f6c-a5ce-09895638197d.jpeg";
    // string objectName = "0a5fb080-1d09-47b7-9183-1b5b89993050.jpeg";

    var objectArgs = new PresignedGetObjectArgs()
        .WithBucket(bucketName)
        .WithObject(objectName)
        .WithExpiry(60 * 60 * 24);

    var result = await minio.PresignedGetObjectAsync(objectArgs);

    Console.WriteLine(result);
}


await GetFileUrl(minio);